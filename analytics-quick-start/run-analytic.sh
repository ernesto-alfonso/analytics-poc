#!/bin/bash -x

AWS=true
AWS=""
if test "${AWS}" = "true"; then
    # AWS
    UAA_URL="https://651b3f38-9af3-4784-8622-304cb9219164.predix-uaa.run.aws-usw02-pr.ice.predix.io"
    ZONEID="ef3c1952-594a-45a3-9fce-08f3e4a391c4"
    CATALOG_URL=https://predix-analytics-catalog-release.run.aws-usw02-pr.ice.predix.io
    ANALYTIC_ID="ff8d0a68-3675-4a2c-a7f7-59a9cd39545f"
else
    # ASV
    UAA_URL="https://e813ef9c-9f07-4df4-a095-065b3fb1cf63.predix-uaa.run.asv-pr.ice.predix.io"
    ZONEID=bd1ae3c8-4d4b-4bac-b72f-ac1fbcf58232
    CATALOG_URL=https://predix-analytics-catalog-release.run.asv-pr.ice.predix.io
    ANALYTIC_ID=3052b539-f8e0-48ac-812f-3ab9a8973765
fi

CLIENTID=framework-client
SECRET="ernesto"

uaac target "${UAA_URL}" || exit ${LINENO}
uaac token client get "${CLIENTID}" -s "${SECRET}" || exit ${LINENO}
TOKEN=$(uaac context | grep access_token: | cut -d: -f2 | tr -d ' ') || exit ${LINENO}

#  list all analytics
curl -i "${CATALOG_URL}/api/v1/catalog/analytics" \
     -H "Authorization: bearer ${TOKEN}" -H "Predix-Zone-Id: ${ZONEID}"

# exit


# run an analytic
curl "${CATALOG_URL}/api/v1/catalog/analytics/${ANALYTIC_ID}/execution" \
	-H "Authorization: bearer ${TOKEN}"\
	-H "Predix-Zone-Id: ${ZONEID}" \
	-H "Content-Type: application/json" \
	-i -d @-\
	<<EOF
[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99]
EOF

<div id="table-of-contents">
<h2>Table of Contents</h2>
<div id="text-table-of-contents">
<ul>
<li><a href="#sec-1">1. obtain a predix account</a></li>
<li><a href="#sec-2">2. install dependencies</a></li>
<li><a href="#sec-3">3. bootstrap the analytics-framework</a></li>
<li><a href="#sec-4">4. create and deploy the analytic</a></li>
<li><a href="#sec-5">5. execute the analytic via REST</a></li>
</ul>
</div>
</div>


# obtain a predix account<a id="sec-1" name="sec-1"></a>

-   api url: `https://api.system.asv-pr.ice.predix.io`
-   username: `ernesto.alfonsogonzalez@ge.com`
-   org: `ernesto.alfonsogonzalez@ge.com`
-   password: `...`

# install dependencies<a id="sec-2" name="sec-2"></a>

-   install uaac
    
        sudo yum install -y gem ruby-devel gcc-c++
        sudo -E gem install cf-uaac
-   install cf-boot
    
        sudo -E pip install cf-boot

# bootstrap the analytics-framework<a id="sec-3" name="sec-3"></a>

Create analytics-framework and uaa service instances.
Create uaa client `framework-client` client with appropriate scopes
-   obtain the project spec: [analytics-boot.json](file:///home/vagrant/git/analytics-poc/predix-analytics-integration/analytics-boot/analytics-boot.json)
-   obtain sample [input.json](file:///home/vagrant/git/analytics-poc/predix-analytics-integration/analytics-boot/input.json), replace placeholders of
    cf-boot input file: 
    
        {
          "CF_PASSWORD": "<MYPASSWORD>",
          "CF_TARGET": "https://api.system.asv-pr.ice.predix.io",
          "CF_SPACE_UAA": "dev",
          "CF_SPACE": "dev",
          "CF_ORG": "<MYORG>",
          "CF_USER": "<MYORG>",
          "analytics-instance-name": "bitstew-analytics-framework",
          "uaa-admin-secret": "<MY_SECRET>",
          "uaa-instance-name": "bitstew-analytics-uaa",
          "framework-client-id": "framework-client",
          "ui-domain-prefix": "bitstew-poc"
        }

-   run bootstrap 
    
        cf-boot analytics-boot.json -i input.json
    
    and get results:
    
        {
          "CF_PASSWORD": "", 
          "CF_TARGET": "https://api.system.aws-usw02-pr.ice.predix.io", 
          "CF_SPACE_UAA": "poc", 
          "CF_SPACE": "poc", 
          "CF_ORG": "erjoalgo@ge.com", 
          "CF_USER": "erjoalgo@ge.com", 
          "framework-instance-name": "bitstew-analytics-framework", 
          "uaa-admin-secret": "ernesto", 
          "uaa-instance-name": "bitstew-analytics-uaa", 
          "framework-client-id": "framework-client", 
          "ui-domain-prefix": "bitstew-poc", 
          "CF_HOME": "/tmp/cf-homes/erjoalgo@ge.com-poc-d086309299a033bb1dbc8dba14233cb8", 
          "uaa_instance_guid": "651b3f38-9af3-4784-8622-304cb9219164", 
          "uaa_uri": "https://651b3f38-9af3-4784-8622-304cb9219164.predix-uaa.run.aws-usw02-pr.ice.predix.io", 
          "uaa_issuer_id": "https://651b3f38-9af3-4784-8622-304cb9219164.predix-uaa.run.aws-usw02-pr.ice.predix.io/oauth/token", 
          "uaa_zone_id": "651b3f38-9af3-4784-8622-304cb9219164", 
          "uaa-dashboard-url": "https://uaa-dashboard.run.aws-usw02-pr.ice.predix.io/#/login/651b3f38-9af3-4784-8622-304cb9219164", 
          "framework-instance-guid": "74734c7a-bd0f-4e12-95d6-ca51526a8aff", 
          "framework-scope": "analytics.zones.74734c7a-bd0f-4e12-95d6-ca51526a8aff.user", 
          "framework-ui-url": "https://bitstew-poc.predix-analytics-ui.run.aws-usw02-pr.ice.predix.io", 
          "framework-catalog_uri": "https://predix-analytics-catalog-release.run.aws-usw02-pr.ice.predix.io"
        }

# create and deploy the analytic<a id="sec-4" name="sec-4"></a>

-   obtain the "framework-ui-url" from the results.
    log in with the test user defined in bootstrap.
-   obtain and build an example analytic
    -   <https://github.com/PredixDev/predix-analytics-sample/tree/master/analytics/demo-adder>
-   upload, deploy, and validate the analytic through the UI

# execute the analytic via REST<a id="sec-5" name="sec-5"></a>

-   obtain a token for the "framework-client" client
    
        uaac target "https://e813ef9c-9f07-4df4-a095-065b3fb1cf63.predix-uaa.run.asv-pr.ice.predix.io"
        uaac token client get framework -s "<MY_SECRET>"
-   obtain the analytic ID
    -   Use "framework-instance-guid" as the ZONEID
    -   Use the "catalog\_uri" as the base URL
    
        TOKEN=$(uaac context | grep access_token: | cut -d: -f2 | tr -d ' ') || exit ${LINENO}
        ZONEID=bd1ae3c8-4d4b-4bac-b72f-ac1fbcf58232
        curl https://predix-analytics-catalog-release.run.asv-pr.ice.predix.io/api/v1/catalog/analytics \
        -H "Authorization: bearer ${TOKEN}" -H "Predix-Zone-Id: ${ZONEID}"
        ...
        {"currentPageSize":1
        "maximumPageSize":25
        "currentPageNumber":0
        "totalElements":1
        "totalPages":1
        "analyticCatalogEntries":[{"id":"52db4215-501a-473c-9080-9acc352c0d80"
        "name":"demo add java"
        "author":"ernesto"
        "description":"demo java adder"
        "version":"v1"
        "supportedLanguage":"Java"
        "customMetadata":null
        "taxonomyLocation":"/uncategorized"
        "state":"VALIDATED"
        "access":"FULL"
        "createdTimestamp":"2017-06-20T03:42:51+00:00"
        "updatedTimestamp":"2017-06-20T03:49:37+00:00"}]}
-   POST data to `<catalog_uri>/api/v1/catalog/analytics/{analitic-id}/execution` and get the result:
    
         ANALYTIC_ID=52db4215-501a-473c-9080-9acc352c0d80
         curl "https://predix-analytics-catalog-release.run.asv-pr.ice.predix.io/api/v1/catalog/analytics/${ANALYTIC_ID}/execution" \
             -H "Authorization: bearer ${TOKEN}"\
             -H "Predix-Zone-Id: ${ZONEID}" \
             -H "Content-Type: application/json" \
             -i -d @-\
             <<EOF
         {"number1": 1900, "number2": 17}
         EOF
        ...
        {"analyticId":"52db4215-501a-473c-9080-9acc352c0d80"
        "status":"COMPLETED"
        "message":"Analytic executed successfully."
        "inputData":"{\"number1\": 1900
         \"number2\": 17}"
        "result":"{\"result\":1917}"
        "createdTimestamp":"2017-06-20 04:18:03.294"
        "updatedTimestamp":"2017-06-20 04:18:04.598"}
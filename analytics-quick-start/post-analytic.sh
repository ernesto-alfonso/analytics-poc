#!/bin/bash -x

CLIENT=framework-client
SECRET="ernesto"

#  show the scopes 

uaac token client get admin -s "${SECRET}";
uaac client get "${CLIENT}"

uaac token client get framework-client -s "ernesto"
TOKEN=$(uaac context | grep access_token: | cut -d: -f2 | tr -d ' ')
ZONEID=bd1ae3c8-4d4b-4bac-b72f-ac1fbcf58232
# http_proxy="" curl http://localhost:1234 \
curl https://predix-analytics-catalog-release.run.asv-pr.ice.predix.io/api/v1/catalog/analytics \
	-H "Authorization: bearer ${TOKEN}"\
	-H "Predix-Zone-Id: ${ZONEID}" \
	-H "Content-Type: application/json" \
	-i -d @-\
	<<EOF
{
"supportedLanguage": "Java",
"author": "Ernesto",
"version": "v1",
"name": "bitstew addition analytic poc"
}
EOF
#  {"id":"66a22c9e-923c-437c-9b31-b8fc032b24bd","name":"bitstew addition analytic poc","author":"Ernesto","description":null,"version":"v1","supportedLanguage":"Java","customMetadata":null,"taxonomyLocation":"/uncategorized","state":null,"access":"FULL","createdTimestamp":"2017-06-19T18:15:49+00:00","updatedTimestamp":"2017-06-19T18:15:49+00:00"}

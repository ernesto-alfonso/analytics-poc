# Automated install

-   The script automates the steps listed in the **Manual install** section below
    
        wget https://bitbucket.org/ernesto-alfonso/analytics-poc/raw/master/analytics-boot/boot/analytics-boot.sh
        chmod +x analytics-boot.sh
        ./analytics-boot.sh

# Manual install

## obtain dependencies

-   install pip, cf-boot
    
        sudo yum install -y python-pip #or brew install python
        pip install --user cf-boot

-   install the cf-cli
    
    <https://docs.cloudfoundry.org/cf-cli/install-go-cli.html>
    
        sudo wget -O /etc/yum.repos.d/cloudfoundry-cli.repo https://packages.cloudfoundry.org/fedora/cloudfoundry-cli.repo
        sudo yum install cf-cli

## obtain cf-boot `analytics-boot.json` and `sample-input.json`

-   <https://bitbucket.org/ernesto-alfonso/analytics-poc/raw/master/analytics-boot/analytics-boot.json>
-   <https://bitbucket.org/ernesto-alfonso/analytics-poc/raw/master/analytics-boot/sample-input.json>

## modify `sample-input.json` values

    vi sample-input.json

-   `sample-input.json` variables doc
    
    <table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">
    
    
    <colgroup>
    <col  class="left" />
    
    <col  class="left" />
    
    <col  class="left" />
    </colgroup>
    <tbody>
    <tr>
    <td class="left">**variable**</td>
    <td class="left">**example**</td>
    <td class="left">**description**</td>
    </tr>
    
    
    <tr>
    <td class="left">&#xa0;</td>
    <td class="left">&#xa0;</td>
    <td class="left">&#xa0;</td>
    </tr>
    
    
    <tr>
    <td class="left">CF\_TARGET</td>
    <td class="left"><https://api.system.aws-usw02-pr.ice.predix.io></td>
    <td class="left">the api endpoint of the CF/predix instance (see "cf target" output)</td>
    </tr>
    
    
    <tr>
    <td class="left">CF\_USER</td>
    <td class="left">me@ge.com</td>
    <td class="left">CF account email</td>
    </tr>
    
    
    <tr>
    <td class="left">CF\_PASSWORD</td>
    <td class="left">******REMOVED******</td>
    <td class="left">CF account password</td>
    </tr>
    
    
    <tr>
    <td class="left">CF\_ORG</td>
    <td class="left">me@ge.com</td>
    <td class="left">CF ORG   (see "cf orgs" output)</td>
    </tr>
    
    
    <tr>
    <td class="left">CF\_SPACE</td>
    <td class="left">dev</td>
    <td class="left">CF SPACE  (see "cf spaces" output)</td>
    </tr>
    
    
    <tr>
    <td class="left">timeseries-instance-name</td>
    <td class="left">timeseries-analytics-poc</td>
    <td class="left">name of the timeseries service instance</td>
    </tr>
    
    
    <tr>
    <td class="left">asset-instance-name</td>
    <td class="left">timeseries-analytics-poc</td>
    <td class="left">name of the asset service instance</td>
    </tr>
    
    
    <tr>
    <td class="left">uaa-admin-secret</td>
    <td class="left">test</td>
    <td class="left">admin secret for the uaa service instance</td>
    </tr>
    
    
    <tr>
    <td class="left">uaa-instance-name</td>
    <td class="left">bitstew-analytics-uaa</td>
    <td class="left">name of the uaa service instance</td>
    </tr>
    
    
    <tr>
    <td class="left">framework-instance-name</td>
    <td class="left">bitstew-analytics-framework</td>
    <td class="left">name of the predix-analytics-framework service  instance</td>
    </tr>
    
    
    <tr>
    <td class="left">framework-client-id</td>
    <td class="left">framework-client</td>
    <td class="left">client for predix-analytics-framework</td>
    </tr>
    
    
    <tr>
    <td class="left">framework-ui-client-id</td>
    <td class="left">ui-client</td>
    <td class="left">client id for the predix-analytics-framework UI</td>
    </tr>
    
    
    <tr>
    <td class="left">framework-ui-domain-prefix</td>
    <td class="left">random-domain-31585</td>
    <td class="left">prefix for the predix-analytics-framework</td>
    </tr>
    
    
    <tr>
    <td class="left">test-user-name</td>
    <td class="left">test</td>
    <td class="left">test user name for predix-analytics-framework UI</td>
    </tr>
    
    
    <tr>
    <td class="left">test-user-email</td>
    <td class="left">test@ge.com</td>
    <td class="left">test user email for predix-analytics-framework UI</td>
    </tr>
    
    
    <tr>
    <td class="left">test-user-pass</td>
    <td class="left">test</td>
    <td class="left">test user password for predix-analytics-framework UI</td>
    </tr>
    </tbody>
    </table>

-   run bootstrap
    
        cf-boot analytics-boot.json -i sample-input.json
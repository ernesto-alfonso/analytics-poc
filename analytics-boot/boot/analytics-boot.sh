#!/bin/bash -x


if ! command -v pip; then
    if command -v yum; then
	sudo yum install -y python-pip
    elif command -v brew; then
	sudo -E easy_install pip || brew install python || exit ${LINENO}
    fi
fi


command -v cf-boot  || pip install --user cf-boot || exit ${LINENO}

if ! command -v cf; then
    # install cf cli, centos-specifc. for other OS, see
    # https://docs.cloudfoundry.org/cf-cli/install-go-cli.html#installer
    if command -v yum; then
	pushd .; cd /tmp
	sudo wget -O /etc/yum.repos.d/cloudfoundry-cli.repo https://packages.cloudfoundry.org/fedora/cloudfoundry-cli.repo
	sudo yum install cf-cli || exit ${LINENO}
	popd
    elif command -v brew; then
	brew tap cloudfoundry/tap || exit ${LINENO}
	brew install cf-cli || exit ${LINENO}
    fi
fi

function valid_json_p {
    python -c 'import json, sys; json.load(sys.stdin)' < "${1}"
}

function maybe_wget_json_file {
    FILENAME=${1}
    shift
    for URL in $*; do
	if ! valid_json_p "${FILENAME}" ; then
	    test -e "${FILENAME}" || rm "${FILENAME}"
	    wget -O "${FILENAME}" "${URL}" && return
	fi
    done
}

SPEC=analytics-boot.json

maybe_wget_json_file "${SPEC}" \
		     https://bitbucket.org/ernesto-alfonso/analytics-poc/raw/master/analytics-boot/analytics-boot.json \
		     https://github.build.ge.com/raw/212556701/predix-analytics-integration-poc/master/analytics-boot/analytics-boot.json \
    || exit ${LINENO}

INPUT=input.json
maybe_wget_json_file "${INPUT}"  \
		     https://bitbucket.org/ernesto-alfonso/analytics-poc/raw/master/analytics-boot/sample-input.json \
		     https://github.build.ge.com/raw/212556701/predix-analytics-integration-poc/master/analytics-boot/sample-input.json



EDITOR=${EDITOR:-${VISUAL:-vi}}

sed -i "s/\(\"ui-domain-prefix\"\): \"bitstew-poc-demo\"/\1: \"random-domain-${RANDOM}\"/" "${INPUT}"

$EDITOR "${INPUT}" || exit ${LINENO}

RESULTS=results.json

cf-boot "${SPEC}" -i "${INPUT}" -o results.json || exit ${LINENO}

test -e "${RESULTS}" || exit ${LINENO}

UI_URL=$(python -c 'import json, sys; print(json.load(sys.stdin)["framework-ui-url"])' < "${RESULTS}") || exit ${LINENO}

echo "visiting ${UI_URL}..."

CHROME="$(command -v chrome || command -v chromium-browser)"

if test -n "${CHROME}"; then
    ${CHROME} "${UI_URL}" || chrome "${UI_URL}" || exit ${LINENO}
else
    echo "visit framwork UI: ${UI_URL}"
fi
